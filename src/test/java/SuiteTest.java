import bst.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AddTests.class,
        ContainsTests.class,
        DeleteTests.class,
        MaxTests.class,
        MinTests.class
})
public class SuiteTest {
}
