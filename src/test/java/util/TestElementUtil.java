package util;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class TestElementUtil {

    public static final List<Integer> elements = Stream.of(4, 2, 3, 1, 7, 5, 11, 9).collect(Collectors.toList());

}
