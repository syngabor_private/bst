package bst;

import org.junit.Assert;
import org.junit.Test;
import util.TestElementUtil;

public class ContainsTests {

    @Test
    public void nonExists() {
        BinarySearchTree bt = new BinarySearchTree();
        TestElementUtil.elements.forEach(bt::add);
        Assert.assertFalse(bt.contains(110));
    }

    @Test
    public void firstLevel() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        Assert.assertTrue(bt.contains(4));
    }

    @Test
    public void secondLevel() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        bt.add(2);
        Assert.assertTrue(bt.contains(2));
    }

    @Test
    public void lastLevel() {
        BinarySearchTree bt = new BinarySearchTree();
        TestElementUtil.elements.forEach(bt::add);
        Assert.assertTrue(bt.contains(9));
    }

}
