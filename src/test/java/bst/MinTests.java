package bst;

import org.junit.Assert;
import org.junit.Test;
import util.TestElementUtil;

public class MinTests {

    @Test
    public void nullElement() {
        BinarySearchTree bt = new BinarySearchTree();
        Assert.assertNull(bt.min());
    }

    @Test
    public void oneELement() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        Assert.assertEquals((Integer) 4, bt.min());
    }

    @Test
    public void twoElement() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        bt.add(2);
        Assert.assertEquals((Integer) 2, bt.min());
    }

    @Test
    public void allElement() {
        BinarySearchTree bt = new BinarySearchTree();
        TestElementUtil.elements.forEach(bt::add);
        Assert.assertEquals((Integer) 1, bt.min());
    }

}
