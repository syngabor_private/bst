package bst;

import org.junit.Assert;
import org.junit.Test;
import util.TestElementUtil;

public class AddTests {

    @Test
    public void oneElement() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        Assert.assertEquals(4, bt.getRoot().getValue());
    }

    @Test
    public void twoElement() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        bt.add(2);
        Assert.assertEquals(2, bt.getRoot().getLeft().getValue());
    }

    @Test
    public void moreElement() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        bt.add(2);
        bt.add(3);
        bt.add(1);
        Assert.assertEquals(3, bt.getRoot().getLeft().getRight().getValue());
        Assert.assertEquals(1, bt.getRoot().getLeft().getLeft().getValue());
    }

    @Test
    public void allElement() {
        BinarySearchTree bt = new BinarySearchTree();
        TestElementUtil.elements.forEach(bt::add);
        TestElementUtil.elements.forEach(item -> Assert.assertTrue(bt.contains(item)));
    }

    @Test
    public void allElementWithPlaceCheck(){
        BinarySearchTree bt = new BinarySearchTree();
        TestElementUtil.elements.forEach(bt::add);
        TestElementUtil.elements.forEach(item -> Assert.assertTrue(bt.contains(item)));
        Assert.assertEquals(4, bt.getRoot().getValue());
        Assert.assertEquals(2, bt.getRoot().getLeft().getValue());
        Assert.assertEquals(3, bt.getRoot().getLeft().getRight().getValue());
        Assert.assertEquals(1, bt.getRoot().getLeft().getLeft().getValue());
        Assert.assertEquals(7, bt.getRoot().getRight().getValue());
        Assert.assertEquals(5, bt.getRoot().getRight().getLeft().getValue());
        Assert.assertEquals(11, bt.getRoot().getRight().getRight().getValue());
        Assert.assertEquals(9, bt.getRoot().getRight().getRight().getLeft().getValue());
    }

}
