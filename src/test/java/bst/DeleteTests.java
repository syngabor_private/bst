package bst;

import org.junit.Assert;
import org.junit.Test;
import util.TestElementUtil;

public class DeleteTests {

    @Test
    public void nonExistsElement() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        bt.del(2);
        Assert.assertEquals(4, bt.getRoot().getValue());
    }

    @Test
    public void oneElement() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        bt.del(4);
        Assert.assertNull(bt.getRoot());
    }

    @Test
    public void twoElement() {
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        bt.add(2);
        bt.del(4);
        bt.del(2);
        Assert.assertNull(bt.getRoot());
    }

    @Test
    public void allElement(){
        BinarySearchTree bt = new BinarySearchTree();
        TestElementUtil.elements.forEach(bt::add);
        TestElementUtil.elements.forEach(bt::del);
        Assert.assertNull(bt.getRoot());
    }

}
