package bst;

import org.junit.Assert;
import org.junit.Test;
import util.TestElementUtil;

public class MaxTests {

    @Test
    public void nonExists(){
        BinarySearchTree bt = new BinarySearchTree();
        Assert.assertNull(bt.max());
    }

    @Test
    public void oneELement(){
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        Assert.assertEquals((Integer) 4, bt.max());
    }

    @Test
    public void twoElement(){
        BinarySearchTree bt = new BinarySearchTree();
        bt.add(4);
        bt.add(2);
        Assert.assertEquals((Integer) 4, bt.max());
    }

    @Test
    public void allElement(){
        BinarySearchTree bt = new BinarySearchTree();
        TestElementUtil.elements.forEach(bt::add);
        Assert.assertEquals((Integer) 11, bt.max());
    }

}
