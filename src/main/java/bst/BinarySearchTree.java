package bst;

public class BinarySearchTree {

    Node root;

    public Node getRoot() {
        return root;
    }

    public Integer min() {
        return min(root);
    }

    public Integer max() {
        return max(root);
    }

    public boolean contains(int value) {
        return contains(root, value);
    }

    public void add(int value) {
        root = add(root, value);
    }

    public void del(int value) {
        root = del(root, value);
    }

    private Integer min(Node root) {
        if (root == null) {
            return null;
        }
        int minimum = root.getValue();
        while (root.getLeft() != null) {
            minimum = root.getLeft().getValue();
            root = root.getLeft();
        }
        return minimum;
    }

    private Integer max(Node root) {
        if (root == null) {
            return null;
        }
        int maximum = root.getValue();
        while (root.getRight() != null) {
            maximum = root.getRight().getValue();
            root = root.getRight();
        }
        return maximum;
    }

    private boolean contains(Node current, int value) {
        if (current == null) {
            return false;
        }
        if (value == current.getValue()) {
            return true;
        }
        return value < current.getValue() ? contains(current.getLeft(), value) : contains(current.getRight(), value);
    }

    private Node add(Node current, int value) {
        if (current == null) {
            return new Node(value);
        }

        if (value < current.getValue()) {
            current.setLeft(add(current.getLeft(), value));
        } else if (value > current.getValue()) {
            current.setRight(add(current.getRight(), value));
        } else {
            return current;
        }

        return current;
    }

    private Node del(Node current, int value) {
        if (current == null) {
            return current;
        }

        if (value < current.getValue()) {
            current.setLeft(del(current.getLeft(), value));
        } else if (value > current.getValue()) {
            current.setRight(del(current.getRight(), value));
        } else {
            if (current.getLeft() == null) {
                return current.getRight();
            } else if (current.getRight() == null) {
                return current.getLeft();
            }
            current.setValue(min(current.getRight()));
            current.setRight(del(current.getRight(), current.getValue()));
        }

        return current;
    }

}
