### Binary Search Tree

#### Build:
- mvn clean install

#### Elements: 
- 4, 2, 3, 1, 7, 5, 11, 9
```
            4
    2               7
1       3       5       11
                    9      
```

#### Methods:
- min()
    - result the minimum value from BST
- max()
    - result the maximum value from BST
- contains(int value)
    - check a value in BST
- add(int value)
    - add a value to BST
- del(int value)
    - delete a value from BST and recalculate the BST
    